import React, {useEffect, useState} from 'react';


function HatForm() {

    const [locations, setLocations] = useState([]);
    const [fabric, setFabric] = useState('');
    const [style, setStyle] = useState('');
    const [color, setColor] = useState('');
    const [imgUrl, setImgUrl] = useState('');
    const [location, setLocation] = useState('');

    const handleFabricChange = event => {
        const value = event.target.value;
        setFabric(value);
    }

    const handleStyleChange = event => {
        const value = event.target.value;
        setStyle(value);
    }

    const handleColorChange = event => {
        const value = event.target.value;
        setColor(value);
    }

    const handleImgUrlChange = event => {
        const value = event.target.value;
        setImgUrl(value);
    }

    const handleLocationChange = event => {
        const value = event.target.value;
        setLocation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.fabric = fabric;
        data.style_name = style;
        data.color = color;
        data.img_url = imgUrl;
        data.location = location;

        console.log(data);

        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            }
        }

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);

            setFabric('');
            setStyle('');
            setColor('');
            setImgUrl('');
            setLocation('');
        }
    }


    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);

        }
    }

    useEffect(() => {
        fetchData();
      }, []);

    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a hat</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input onChange={handleFabricChange} value={fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStyleChange} value={style} placeholder="Style" required type="text" name="style_name" id="style_name" className="form-control"  />
                <label htmlFor="style">Style</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleImgUrlChange} value={imgUrl} placeholder="Image" required type="text" name="img_url" id="img_url" className="form-control" />
                <label htmlFor="img_url">Image URL</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} value={location} required name="location" id="location" className="form-select" >
                <option value="">Choose a closet</option>
                {locations.map(location => {
                    return (
                    <option key={location.id} value={location.id}>
                        {location.closet_name}
                    </option>
                    );
                })}
                </select>
              </div>
              <button className="btn btn-primary">Add</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default HatForm;
