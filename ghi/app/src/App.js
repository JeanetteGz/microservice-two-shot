import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import HatForm from './HatForm';
import HatList from './HatList';
import Hat from './Hat';
import Nav from './Nav';
import ShoeForm from './ShoeForm';
import ShoeList from './ShoeList';


function App(props) {
  if (props.hats === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route path="/hats" element={<HatList hats={props.hats}/>} />
            <Route path=":hatId" element={<Hat />} />
            <Route path="new" element={<HatForm />} />
          </Route>
          <Route path="shoes">
            <Route path="/shoes" element={<ShoeList shoes={props.shoes}/>} />
            <Route path="new" element={<ShoeForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
