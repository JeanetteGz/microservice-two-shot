import { NavLink, Outlet } from 'react-router-dom'
import HatForm from './HatForm';
import {useState} from 'react'

function HatList(props) {

    const [hats, setHats] = useState(props.hats);

    const handleDelete = async (hatId) => {
      console.log(hatId)
       const hatUrl  = `http://localhost:8090/api/hats/${hatId}`;
       const fetchConfig = {
                method: "delete"
    }
    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
      console.log("hat deleted");
      const newHats = hats.filter(hat => {
        return (
          hat.id !== hatId
        )
      })
      setHats(newHats)
    }
    }


    return(
        <>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Fabric</th>
            <th>Style Name</th>
            <th>Color</th>
            <th>Closet Name</th>
            <th>Image</th>
            <th>Delete</th>
          </tr>
          </thead>
          <tbody>
        {hats.map(({id, fabric, style_name, color, location, img_url}) => {
          return (
            <tr key={id}>
            <td>{ fabric }</td>
            {/* <NavLink to={`/hats${hat.href}`} key={hatId} style={{listStyle: 'disc'}}> */}
            <td>{ style_name }</td>
            {/* </NavLink> */}
            <td>{ color }</td>
            <td>{ location.closet_name }</td>
            <td><img src={`${ img_url }`} width="100" height="100" /></td>
            <td><button className="btn btn-primary" onClick={() => handleDelete(id)}>Delete</button></td>
            </tr>
            )})}
    </tbody>
    </table>
    <div>
    <NavLink to="new" element={<HatForm />}><button className= "btn btn-primary">Add a hat</button></NavLink>
    </div>
    </>
    )
}

    export default HatList;
