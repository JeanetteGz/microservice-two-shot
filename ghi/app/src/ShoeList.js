import { NavLink, Outlet } from 'react-router-dom'

function ShoeList(props) {

  const handleDelete = (id) => {
    fetch(`/api/shoes/${id}`, {
      method: 'DELETE',
    })
  }

  return(
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Model Name</th>
          <th>Shoe</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>
        {props.shoes.map(shoe => {
          return (
            <tr key={shoe.href}>
              <td>{ shoe.model_name }</td>
              <td>
                <img src={shoe.picture_url} alt={shoe.model_name}  style={{ width: "100px", height: "100px" }} />
              </td>
              <td>{ shoe.manufacturer }</td>
              <td>{ shoe.bin }</td>
              <td>{ shoe.color }</td>
              <td>{ shoe.id }</td>
              <td>
                <button onClick={() => handleDelete(shoe.id)}>Delete</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}


    export default ShoeList;
