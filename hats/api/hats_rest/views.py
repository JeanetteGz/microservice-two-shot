from django.shortcuts import render
from .models import LocationVO, Hat
import json
from common.json import ModelEncoder
from django.http import JsonResponse


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href", "closet_name"]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["id", "fabric", "style_name", "color", "img_url", "location"]

    encoders = {"location": LocationVOEncoder()}


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "img_url",
        "location",
        ]
    encoders = {
        "location": LocationVOEncoder()
        }


def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        location = content["location"]

        try:
            location_href = f'/api/locations/{location}/'
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


def api_show_hat(request, id):
    if request.method == "GET":
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    else:
        request.method == "DELETE"
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
